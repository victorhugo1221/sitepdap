<?php  include('header.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Enter your description here" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Primary Meta Tags -->
    <title>Porto das Águas - Carrinho</title>
    <meta name="title" content="Porto das Águas - Atrações">
    <meta name="description" content="Uma breve descrição de 255 caracteres.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="Porto das Águas - Atrações">
    <meta property="og:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="og:image" content="">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="">
    <meta property="twitter:title" content="Porto das Águas - Atrações">
    <meta property="twitter:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="twitter:image" content="">
</head>
 
<main>
    <section class="attraction-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    Carrinho
                </div>
            </div>
        </div>
    </section>
    <section class="container">
     <div class="row">
                
            <!--Section: Block Content-->
<section>

<!--Grid row-->
<div class="row">

  <!--Grid column-->
  <div class="col-lg-8">

    <!-- Card -->
    <div class="card wish-list mb-3">
      <div class="card-body">

        <h5 class="mb-4">Cart (<span>2</span> items)</h5>

        <div class="row mb-4">
          <div class="col-md-5 col-lg-3 col-xl-3">
            <div class="view zoom overlay z-depth-1 rounded mb-3 mb-md-0">
              <img class="img-fluid w-100"
                src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/12a.jpg" alt="Sample">
              <a href="#!">
                <div class="mask waves-effect waves-light">
                  <img class="img-fluid w-100"
                    src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/12.jpg">
                  <div class="mask rgba-black-slight waves-effect waves-light"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-7 col-lg-9 col-xl-9">
            <div>
              <div class="d-flex justify-content-between">
                <div>
                  <h5>Blue denim shirt</h5>
                  <p class="mb-3 text-muted text-uppercase small">Shirt - blue</p>
                  <p class="mb-2 text-muted text-uppercase small">Color: blue</p>
                  <p class="mb-3 text-muted text-uppercase small">Size: M</p>
                </div>
                <div>
                  <div class="def-number-input number-input safari_only mb-0 w-100">
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                      class="minus"></button>
                    <input class="quantity" min="0" name="quantity" value="1" type="number">
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                      class="plus"></button>
                  </div>
                  <small id="passwordHelpBlock" class="form-text text-muted text-center">
                    (Note, 1 piece)
                  </small>
                </div>
              </div>
              <div class="d-flex justify-content-between align-items-center">
                <div>
                  <a href="#!" type="button" class="card-link-secondary small text-uppercase mr-3"><i
                      class="fas fa-trash-alt mr-1"></i> Remove item </a>
                  <a href="#!" type="button" class="card-link-secondary small text-uppercase"><i
                      class="fas fa-heart mr-1"></i> Move to wish list </a>
                </div>
                <p class="mb-0"><span><strong>$17.99</strong></span></p>
              </div>
            </div>
          </div>
        </div>
        <hr class="mb-4">
        <div class="row mb-4">
          <div class="col-md-5 col-lg-3 col-xl-3">
            <div class="view zoom overlay z-depth-1 rounded mb-3 mb-md-0">
              <img class="img-fluid w-100"
                src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/13a.jpg" alt="Sample">
              <a href="#!">
                <div class="mask waves-effect waves-light">
                  <img class="img-fluid w-100"
                    src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/13.jpg">
                  <div class="mask rgba-black-slight waves-effect waves-light"></div>
                </div>
              </a>
          </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Card -->

    <!-- Card -->
    <div class="card mb-3">
      <div class="card-body">

        <h5 class="mb-4">Data de uso</h5>

        <p class="mb-0"> 12/12/2021</p>
      </div>
    </div>
    <!-- Card -->



  </div>
  <!--Grid column-->

  <!--Grid column-->
  <div class="col-lg-4">

    <!-- Card -->
    <div class="card mb-3">
      <div class="card-body">

        <h5 class="mb-3">The total amount of</h5>

        <ul class="list-group list-group-flush">
          <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
            Temporary amount
            <span>$25.98</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center px-0">
            Shipping
            <span>Gratis</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
            <div>
              <strong>The total amount of</strong>
              <strong>
                <p class="mb-0">(including VAT)</p>
              </strong>
            </div>
            <span><strong>$53.98</strong></span>
          </li>
        </ul>

        <button type="button" class="btn btn-primary btn-block waves-effect waves-light">go to checkout</button>

      </div>
    </div>
    <!-- Card -->

    <!-- Card -->
    <div class="card mb-3">
      <div class="card-body">

        <a class="dark-grey-text d-flex justify-content-between" data-toggle="collapse" href="#collapseExample1"
          aria-expanded="false" aria-controls="collapseExample1">
          Add a discount code (optional)
          <span><i class="fas fa-chevron-down pt-1"></i></span>
        </a>

        <div class="collapse" id="collapseExample1">
          <div class="mt-3">
            <div class="md-form md-outline mb-0">
              <input type="text" id="discount-code1" class="form-control font-weight-light"
                placeholder="Enter discount code">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Card -->

  </div>
  <!--Grid column-->

</div>
<!--Grid row-->    
  </div>
</section>
            
    
<?php  include('footer.php'); ?>

<body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/js/bootstrap.min.js"></script>
</body>

</html>