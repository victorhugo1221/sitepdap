
<?php  include('header.php'); ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="Description" content="Enter your description here" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <!-- Primary Meta Tags -->
  <title>Parque Porto das Águas</title>
  <meta name="title" content="Porto das Águas">
  <meta name="description" content="Uma breve descrição de 255 caracteres.">

  <!-- Open Graph / Facebook -->
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:title" content="Porto das Águas">
  <meta property="og:description" content="Uma breve descrição de 255 caracteres.">
  <meta property="og:image" content="">

  <!-- Twitter -->
  <meta property="twitter:card" content="summary_large_image">
  <meta property="twitter:url" content="">
  <meta property="twitter:title" content="Porto das Águas">
  <meta property="twitter:description" content="Uma breve descrição de 255 caracteres.">
  <meta property="twitter:image" content="">

</head>

<body>
  
  <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
        aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
        aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
        aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/img/carousel-1.png" class="d-block w-100" alt="...">
        <div class="carousel-caption h-100 align-items-center justify-content-center d-flex">
          <h5>Embarque nessa diversão!</h5>
        </div>
      </div>
      <div class="carousel-item">
        <img src="/assets/img/carousel-1.png" class="d-block w-100" alt="...">
        <div class="carousel-caption h-100 align-items-center justify-content-center d-flex">
          <h5>Diversas Atrações para todas idades</h5>
        </div>
      </div>
      <div class="carousel-item">
        <img src="/assets/img/carousel-1.png" class="d-block w-100" alt="...">
        <div class="carousel-caption h-100 align-items-center justify-content-center d-flex">
          <h5>Passaporte verão</h5>
        </div>
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
  <main>
    <section class="container">
      <div class="row mt-5 mb-5 p-4">
        <div class="col-md-6">
          <h1 class="h2 color-default">O Parque Aquatico mais divertido da região</h1>
          <p class="description">O Parque Aquático Porto das Águas é um dos principais atrativos turísticos de Santa Catarina. Com atrações para toda a família,
             o parque oferece toda estrutura necessária para você passar momentos de muita alegria e diversão.
            São diversas atrações como toboáguas, kamikases, free-falls, além de um Centro Gastronômico e uma linda área verde. 
            Passe momentos inesquecíveis com sua família e amigos no Parque Aquático Porto das Águas!</p>
          <a href="#" class="more">Ver mais <img src="/assets/img/icons/next-sm.svg" alt="" srcset=""></a>
        </div>
        <div class="col-md-6">
          <div class="emphasis d-flex">
          <video width="100%" height="70%" controls>
          <source src="../assets/img/parque.mp4" type="video/mp4">
           Your browser does not support the video tag.
          </video>
          </div>
        </div>
      </div>
    </section>
    <section class="home-section mt-5 mb-5">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <h2>A diversão garantida da sua familia</h2>
                  <a href="comprar.php" class="btn btn-lg btn-ticket">COMPRE SEU PASSAPORTE</a>
              </div>
          </div>
      </div>
    </section>
    <section class="container">
      <div class="row">
        <div class="col-md-12 text-center mb-5 mt-3">
          <h2 class="color-default">Conheça nossas Atrações</h2>
        </div>
        <div class="col-md-4">
          <div class="card toys">
            <img src="/assets/img/avalanche.jpeg" class="card-img-top" alt="...">
            <div class="card-body text-center">
              <h5 class="card-title"> Half Pipe Avalanche </h5>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card toys">
            <img src="/assets/img/carousel-2.jpeg" class="card-img-top" alt="...">
            <div class="card-body text-center">
              <h5 class="card-title">Kamikaze </h5>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card toys">
            <img src="/assets/img/bolha.jpeg" class="card-img-top" alt="...">
            <div class="card-body text-center">
              <h5 class="card-title">Piscina com Bolha</h5>
            </div>
          </div>
        </div>
        <div class="col-md-12 text-center mt-5">
          <a class="more" href="atracoes.php">Ver todas Atrações <img src="/assets/img/icons/next-sm.svg" alt="" srcset=""></a>
        </div>
      </div>
    </section>

    <?php  include('footer.php'); ?>
    <?php  include('whatsicon.html'); ?>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/js/bootstrap.min.js"></script>
</body>

</html>