<!-- MENU SUPERIOR -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-default p-0">
    <div class="container-fluid ms-5">
      <a class="navbar-brand ms-5 me-5" href="index.php"><img src="/assets/img/brand/logo-sm.png" class="img-fluid logo-nav"
          alt="Porto das Águas" srcset=""></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav justify-content-center ps-5">
          <li class="nav-item ps-4">
            <a class="nav-link active" aria-current="page" href="index.php">Inicio</a>
          </li>
          <li class="nav-item ps-4">
            <a class="nav-link" href="atracoes.php">Atrações</a>
          </li>
          <li class="nav-item ps-4">
            <a class="nav-link" href="hoteis.php">Hotéis</a>
          </li>
          <li class="nav-item ps-4">
            <a class="nav-link" href="#">O parque</a>
          </li>
          <li class="nav-item ps-4">
            <a class="nav-link" href="passaporteverao.php">Passaporte Verão</a>
          </li>
          <li class="nav-item ps-4">
            <div class="nav-link">
              <a href="comprar.php" class="bg-light color-default text-decoration-none p-2 rounded">Comprar</a>
            </div>
          </li>
          <li class="nav-item carrinho-mobile">
            <div class="nav-link">
              <a href="#" class="bg-light color-default text-decoration-none p-2 rounded">
                <img src="/assets/img/icons/cart.svg" alt="Carrinho" class="" srcset="">
              </a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>