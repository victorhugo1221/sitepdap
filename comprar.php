<?php  include('header.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Enter your description here" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Primary Meta Tags -->
    <title>Porto das Águas - Comprar</title>
    <meta name="title" content="Porto das Águas - Descrição">
    <meta name="description" content="Uma breve descrição de 255 caracteres.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="Porto das Águas - Descrição">
    <meta property="og:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="og:image" content="">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="">
    <meta property="twitter:title" content="Porto das Águas - Descrição">
    <meta property="twitter:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="twitter:image" content="">
</head>

<main>
    <section class="ticket-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    Garanta seu Passaporte
                </div>
            </div>
        </div>
    </section>
    <section class="container mt-3">
        <div class="row">
            <div class="col-md-12 mt-4 mb-4 text-center">
                <h2 class="color-default">Selecione uma data e um passaporte</h2>
            </div>
        </div>
    </section>
    <section class="container mt-3">
        <div class="row">
            <div class="col-md-6">
                <div class="calendar">
                    <div class="calendar-head">
                        <!-- MÊS -->
                        <h5>Outubro</h5>
                    </div>
                    <div class="calendar-week">
                        <!-- DIAS DA SEMANA -->
                        <table>
                            <tr>
                                <th>seg</th>
                                <th>ter</th>
                                <th>qua</th>
                                <th>qui</th>
                                <th>sex</th>
                                <th>sab</th>
                                <th>dom</th>
                            </tr>
                            <!-- 1º SEMANA -->
                            <tr>
                                <td>
                                    <div class="day">1</div>
                                </td>
                                <td>
                                    <div class="day">2</div>
                                </td>
                                <td>
                                    <div class="day">3</div>
                                </td>
                                <td>
                                    <div class="day">4</div>
                                </td>
                                <td>
                                    <div class="day">5</div>
                                </td>
                                <td>
                                    <div class="day">6</div>
                                </td>
                                <td>
                                    <div class="day">7</div>
                                </td>
                            </tr>
                            <!-- 2º SEMANA -->
                            <tr>
                                <td>
                                    <div class="day">8</div>
                                </td>
                                <td>
                                    <div class="day">9</div>
                                </td>
                                <td>
                                    <div class="day">10</div>
                                </td>
                                <td>
                                    <div class="day-close">11</div>
                                </td>
                                <td>
                                    <div class="day">12</div>
                                </td>
                                <td>
                                    <div class="day">13</div>
                                </td>
                                <td>
                                    <div class="day">14</div>
                                </td>
                            </tr>
                            <!-- 3º SEMANA -->
                            <tr>
                                <td>
                                    <div class="day">15</div>
                                </td>
                                <td>
                                    <div class="day-close">16</div>
                                </td>
                                <td>
                                    <div class="day">17</div>
                                </td>
                                <td>
                                    <div class="day">18</div>
                                </td>
                                <td>
                                    <div class="day">19</div>
                                </td>
                                <td>
                                    <div class="day">20</div>
                                </td>
                                <td>
                                    <div class="day">21</div>
                                </td>
                            </tr>
                            <!-- 4º SEMANA -->
                            <tr>
                                <td>
                                    <div class="day">22</div>
                                </td>
                                <td>
                                    <div class="day">23</div>
                                </td>
                                <td>
                                    <div class="day">24</div>
                                </td>
                                <td>
                                    <div class="day">25</div>
                                </td>
                                <td>
                                    <div class="day">26</div>
                                </td>
                                <td>
                                    <div class="day">27</div>
                                </td>
                                <td>
                                    <div class="day">28</div>
                                </td>
                            </tr>
                            <!-- 5º SEMANA -->
                            <tr>
                                <td>
                                    <div class="day">29</div>
                                </td>
                                <td>
                                    <div class="day">30</div>
                                </td>
                                <td>
                                    <div class="day">31</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="caption mt-5">
                        <h5>Legenda</h5>
                        <div><img src="/assets/img/icons/caption-1.svg" alt="" class="me-3" srcset="">Parque Fechado
                        </div>
                        <div><img src="/assets/img/icons/caption-2.svg" alt="" class="me-3" srcset="">Parque Aberto
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h5 class="text-center">Ingresso</h5>
                <div class="row mt-5">
                    <div class="col-3"><img src="/assets/img/icons/ticket.svg" alt="" srcset=""></div>
                    <div class="col-3">
                        <h5>Adulto</h5>
                    </div>
                    <div class="col-3">
                        <h5>R$ 65,00</h5>
                    </div>
                    <div class="col-3">
                        <div class="bg-ticket align-items-center justify-content-center d-flex">
                            <button id="menosadult" class="me-2">-</button>
                            <input type="number" name="adult" value="0" min="0" max="50" id="adult">
                            <button id="maisadult" class="ms-2">+</button>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-3"><img src="/assets/img/icons/ticket.svg" alt="" srcset=""></div>
                    <div class="col-3">
                        <h5>Criança</h5>
                    </div>
                    <div class="col-3">
                        <h5>R$ 45,00</h5>
                    </div>
                    <div class="col-3">
                        <div class="bg-ticket align-items-center justify-content-center d-flex">
                            <button id="menosadult" class="me-2">-</button>
                            <input type="number" name="adult" value="0" min="0" max="50" id="adult">
                            <button id="maisadult" class="ms-2">+</button>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-3"><img src="/assets/img/icons/ticket.svg" alt="" srcset=""></div>
                    <div class="col-3">
                        <h5>Senior</h5>
                    </div>
                    <div class="col-3">
                        <h5>R$ 37,00</h5>
                    </div>
                    <div class="col-3">
                        <div class="bg-ticket align-items-center justify-content-center d-flex">
                            <button id="menosadult" class="me-2">-</button>
                            <input type="number" name="adult" value="0" min="0" max="50" id="adult">
                            <button id="maisadult" class="ms-2">+</button>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <h5>Total: R$<span id="total">0</span>,00 </h5>
                </div>
                <div class="d-flex align-content-center justify-content-center align-items-center">
                    <button class="btn btn-default ps-5 pe-5">Continuar</button>
                </div>
            </div>
        </div>
    </section>
    <?php  include('footer.php'); ?>
    <?php  include('whatsicon.html'); ?>
<body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/js/bootstrap.min.js"></script>
</body>

</html>