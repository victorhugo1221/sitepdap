<?php  include('header.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Enter your description here" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Primary Meta Tags -->
    <title>Porto das Águas - Atrações</title>
    <meta name="title" content="Porto das Águas - Atrações">
    <meta name="description" content="Uma breve descrição de 255 caracteres.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="Porto das Águas - Atrações">
    <meta property="og:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="og:image" content="">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="">
    <meta property="twitter:title" content="Porto das Águas - Atrações">
    <meta property="twitter:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="twitter:image" content="">
</head>
 
<main>
    <section class="attraction-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    Conheça nossas Atrações
                </div>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-md-12 mt-5 mb-5 text-center">
                <h2 class="text-red">Brinquedos Radicais</h2>
            </div>
            <!-- MODELO DE POSTAGEM PARA LOOPING -->
            <div class="col-md-12">
                <div class="card mb-3 card-toys">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="/assets/img/carousel-2.jpeg" class="img-fluid rounded-start" alt="...">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Kamikaze</h5>
                          <p class="card-text text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's </p>
                          <p class="card-text"><small class="text-age">Idade Minima: 10 anos </small></p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="col-md-12">
                <div class="card mb-3 card-toys">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="/assets/img/carousel-2.jpeg" class="img-fluid rounded-start" alt="...">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Caracol</h5>
                          <p class="card-text text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's </p>
                          <p class="card-text"><small class="text-age">Idade Minima: 10 anos </small></p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="col-md-12">
                <div class="card mb-3 card-toys">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="/assets/img/avalanche.jpeg" class="img-fluid rounded-start" alt="...">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Half Pipe Avalanche</h5>
                          <p class="card-text text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's </p>
                          <p class="card-text"><small class="text-age">Idade Minima: 12 anos </small></p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>





            <div class="col-md-12 mt-5 mb-5 text-center">
                <h2 class="color-default">Para todas as Idades</h2>
            </div>
            <!-- MODELO DE POSTAGEM PARA LOOPING -->
            <div class="col-md-12">
                <div class="card mb-3 card-toys">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="/assets/img/example-card.png" class="img-fluid rounded-start" alt="...">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Brinquedo 0</h5>
                          <p class="card-text text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's </p>
                          <p class="card-text"><small class="text-age">Idade Minima: 14 anos </small></p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="col-md-12">
                <div class="card mb-3 card-toys">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="/assets/img/carousel-2.jpeg" class="img-fluid rounded-start" alt="...">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Brinquedo 0</h5>
                          <p class="card-text text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's </p>
                          <p class="card-text"><small class="text-age">Idade Minima: 14 anos </small></p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="col-md-12 mt-5 mb-5 text-center">
                <h2 class="text-orange">Kids</h2>
            </div>
            <!-- MODELO DE POSTAGEM PARA LOOPING -->
            <div class="col-md-12">
                <div class="card mb-3 card-toys">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="/assets/img/carousel-2.jpeg" class="img-fluid rounded-start" alt="...">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Brinquedo 0</h5>
                          <p class="card-text text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's </p>
                          <p class="card-text"><small class="text-age">Idade Minima: 5 anos </small></p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="col-md-12">
                <div class="card mb-3 card-toys">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="/assets/img/carousel-2.jpeg" class="img-fluid rounded-start" alt="...">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Brinquedo 0</h5>
                          <p class="card-text text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's </p>
                          <p class="card-text"><small class="text-age">Idade Minima: 5 anos </small></p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="col-md-12">
                <div class="card mb-3 card-toys">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="/assets/img/carousel-2.jpeg" class="img-fluid rounded-start" alt="...">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Brinquedo 0</h5>
                          <p class="card-text text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's Ipsum has been the industry's </p>
                          <p class="card-text"><small class="text-age">Idade Minima: 5 anos </small></p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </section>
    <?php  include('footer.php'); ?>

    <?php  include('whatsicon.html'); ?>


<body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/js/bootstrap.min.js"></script>
</body>

</html>