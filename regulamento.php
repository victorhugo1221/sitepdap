<?php  include('header.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Enter your description here" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Primary Meta Tags -->
    <title>Porto das Águas - Regulamento</title>
    <meta name="title" content="Porto das Águas - Atrações">
    <meta name="description" content="Uma breve descrição de 255 caracteres.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="Porto das Águas - Atrações">
    <meta property="og:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="og:image" content="">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="">
    <meta property="twitter:title" content="Porto das Águas - Atrações">
    <meta property="twitter:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="twitter:image" content="">
</head>
<div class="container">
<div class="col-md-10 text-left">
    <h2>REGULAMENTO</h2>
    <h4>AGÊNCIAS DE TURISMO</h4>
    <p>As melhores agências de turismo trabalham em parceria com o Porto das Águas, procure uma agência e se informe sobre pacotes
        e excursões para o parque que está cada vez melhor! Ofereça ao seu cliente um dos melhores parques aquáticos do Brasil,
        o parque tem localização privilegiada próximo as mais belas praias da região com um tarifário atraente que agrega grande 
        valor ao pacote ou excursão.</p>
    
    <h4>LOCALIZAÇÃO</h4>
    <p>O parque aquático Porto das Águas, está localizado à margem da principal via de acesso das praias de Porto Belo e Bombinhas,
         no km 02 da cidade de Porto Belo. No endereço, Av. Governador Celso Ramos, 1499, Bairro Perequê, Porto Belo-SC. CEP – 88210-000.</p>
    
    <h4>ESTRUTURA</h4>
    <p>O Parque Aquático Porto das Águas destina-se à prática de atividades aquáticas, lazer e condicionamento físico. 
        O parque é composto por suas piscinas, tobogãs, free-falls e kamikases, além de uma quadra poliesportiva,
        um Centro Gastronômico, dois bares molhados e uma ilha.</p>

    <h4>PROIBIDO</h4>
    <p>É explicitamente proibido ingressar ao parque portanto qualquer bebida(s) ou alimento(s)</p>

 
    <h4>PISCINAS</h4>
    <p>Pessoas com ferimentos não poderão utilizar a piscina; Em caso de trovoadas, raios e/ou ventos fortes, as piscinas terão seu funcionamento interrompido.</p>

    <h4>TOBOÁGUAS</h4>
    <h5>PARA SUA SEGURANÇA, NOS TOBOÁGUAS NÃO É PERMITIDO:</h5>
    <p>* Pessoas com ferimentos;
        * Utilizar relógios, óculos, pulseiras, anéis, piercing e acessórios em geral;
        * Bermudas jeans ou similares, apenas de tactel, lycra e nylon.
        * Bermudas ou trajes de banho com zíperes, botões ou adereços de metal.
        ATENÇÃO: Em caso de condições climáticas adversas, como raios, trovoadas e ventos fortes, os brinquedos terão seu funcionamento interrompido.</p>

    </div>
</div>
    <?php  include('footer.php'); ?>

<?php  include('whatsicon.html'); ?>