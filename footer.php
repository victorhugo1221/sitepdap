<section class="container">
      <div class="row mt-5 mb-5">
        <div class="col-md-8">
          <h3 class="color-default-secondary">Deixe seu e-mail e receba todas promoções do parque </h3>
        </div>
        <div class="col-md-4 d-flex">
          <input type="text" class="form-control" name="" id="" placeholder="Digite seu e-mail...">
          <button type="submit" class="btn btn-default ms-2"><img src="/assets/img/icons/next-lg.svg" alt=""
              srcset=""></button>
        </div>
      </div>
    </section>
  </main>
  <footer class="bg-default">
    <div class="container" style="padding-top: 40px;">
      <div class="row">
        <div class="col-md-3">
          <img src="/assets/img/brand/logo.png" alt="" srcset="">
        </div>
        <div class="col-md-3">
          <h4>Links</h4>
          <div><a href="comprar.php" class="text-decoration-none text-white">Calendario do Parque</a></div>
          <div><a href="#" class="text-decoration-none text-white">Politica de Privacidade</a></div>
          <div><a href="#" class="text-decoration-none text-white">Termos de Compra</a></div>
          <div><a href="regulamento.php" class="text-decoration-none text-white">Regulamento do Parque</a></div>
        </div>
        <div class="col-md-3">
          <h4>Localização</h4>
          <p>Av. Gov. Celso Ramos, 1499 - Jardim Dourado, Porto Belo - SC, 88210-000</p>
        </div>
        <div class="col-md-3">
          <h4>Contato</h4>
          <h5>47 3369-8000</h5>
          <h5>47 99787-5397</h5>
          <a class="text-decoration-none text-white"
            href="mailto:contato@parqueportodasaguas.com.br">contato@parqueportodasaguas.com.br</a>
          <div class="d-flex mt-2">
            <a href="https://instagram.com/parqueaquaticoportodasaguas" target="_blank" ><img src="/assets/img/icons/insta.svg" alt="" srcset=""></a>
            <a href="https://facebook.com/parqueaquaticoportodasaguas" target="_blank" class="ms-2"><img src="/assets/img/icons/face.svg" alt="" srcset=""></a>
          </div>
        </div>
        <div class="col-md-12 text-center">
          <h6>2021 © Parque Porto das Águas</h6> 
      </div>
      </div>
    </div>
    </div>
  </footer>