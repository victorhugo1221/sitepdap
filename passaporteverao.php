<?php  include('header.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Enter your description here" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Primary Meta Tags -->
    <title>Porto das Águas - Passaporte Verão</title>
    <meta name="title" content="Porto das Águas - Atrações">
    <meta name="description" content="Uma breve descrição de 255 caracteres.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="Porto das Águas - Atrações">
    <meta property="og:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="og:image" content="">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="">
    <meta property="twitter:title" content="Porto das Águas - Atrações">
    <meta property="twitter:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="twitter:image" content="">
</head>
 
<main>
    <section class="attraction-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    Diversão para todo verão
                </div>
            </div>
        </div>
    </section>
            <section class="container">
              <div class="row">
                <div class="col-md-12 text-center mb-5 mt-3">
                  <h2 class="color-default">Passaporte verão 2021/2022</h2>
                </div>
                <div class="col-md-3">
                  <div class="card toys">
                    <div class="card-body text-center">
                      <h3> 1 Pessoa</h3>
                      <h5 class="card-title"> 10x R$25,80 </h5>
                      <button type="button" class="btn btn-primary">Comprar</button>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card toys">
                    <div class="card-body text-center">
                      <h3> 3 Pessoas</h3>
                      <h5 class="card-title">10x R$60,90</h5>
                      <button type="button" class="btn btn-primary">Comprar</button>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card toys">
                    <div class="card-body text-center">
                      <h3> 4 Pessoas</h3>
                      <h5 class="card-title">10x R$75,40</h5>
                      <button type="button" class="btn btn-primary">Comprar</button>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card toys">
                    <div class="card-body text-center">
                      <h3> 5 Pessoas</h3>
                      <h5 class="card-title"> 10x R$87,80</h5>
                      <button type="button" class="btn btn-primary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            
<?php  include('footer.php'); ?>
<?php  include('whatsicon.html'); ?>

<body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/js/bootstrap.min.js"></script>
</body>

</html>