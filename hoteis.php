<?php  include('header.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Enter your description here" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Primary Meta Tags -->
    <title>Porto das Águas - Regulamento</title>
    <meta name="title" content="Porto das Águas - Atrações">
    <meta name="description" content="Uma breve descrição de 255 caracteres.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="Porto das Águas - Atrações">
    <meta property="og:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="og:image" content="">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="">
    <meta property="twitter:title" content="Porto das Águas - Atrações">
    <meta property="twitter:description" content="Uma breve descrição de 255 caracteres.">
    <meta property="twitter:image" content="">
</head>
<main>
    <section class="hotel-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    Hospedagem
                </div>
            </div>
        </div>
    </section>
            <section class="container">
              <div class="row">
                <div class="col-md-12 text-center mb-5 mt-3">
                  <h2 class="color-default">Hotéis parceiros</h2>
                </div>
                <div class="col-md-3">
                  <div class="card toys">
                    <img src="/assets/img/pousada-pe-na-areia.jpg" class="img-thumbnail" alt="...">
                    <div class="card-body text-center">
                      <h4> Pousada pé na Areia</h4>
                      <h6> <i class="fas fa-map-marker-alt"></i> Penha-SC</h6>
                      <h6> <i class="fas fa-phone-alt"></i> 47 3345-5201</h6>
                      <p> A Pousada Pé na Areia está localizada de frente para o mar, na Praia do Trapiche em Penha.
                         A praia em frente a pousada é um charme a parte,  águas calmas, com uma visual incrível da orla.</p>
                      <a href="https://pousadapenaareia.tur.br/" target="_blank"> <button type="button" class="btn btn-primary">Reservar </button></a> 
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card toys">
                    <img src="/assets/img/summerbeach.jpg" class="img-thumbnail" alt="...">
                    <div class="card-body text-center">
                      <h4> Bombinhas Summer Beach</h4>
                      <h6> <i class="fas fa-map-marker-alt"></i> Bombinhas-SC</h6>
                      <h6> <i class="fas fa-phone-alt"></i> (47) 3393-9700</h6>
                      <p> Há apenas 300 metros da praia de bombas, o Bombinhas Summer Beach hotel & spa reúne tudo o que você precisa para ter uma 
                        estadia perfeita, repleta de momentos inesquecíveis.</p>
                        <a href="https://www.bombinhassummerbeach.com.br/" target="_blank"> <button type="button" class="btn btn-primary">Reservar </button></a> 
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card toys">
                    <img src="/assets/img/morro-do-sol.jpg" class="img-thumbnail" alt="...">
                    <div class="card-body text-center">
                      <h4> Hotel Morro do Sol</h4>
                      <h6> <i class="fas fa-map-marker-alt"></i> Porto Belo-SC</h6>
                      <h6> <i class="fas fa-phone-alt"></i> (47) 3369-4566</h6>
                      <p>O Hotel dispõe de 59 acomodações integradas a um espaço de lazer, segurança e tranquilidade,para você e sua família. 
                        Conta com 3 piscinas sendo uma delas aquecida e coberta, sala de jogos, quadras de esporte, área de churrasqueira, playground e Restaurante Panorâmico</p>
                        <a href="https://www.vbhoteiseeventos.com.br/morrodosolhoteleeventos?gclid=EAIaIQobChMIkPeoqsH18wIVhw-RCh1l-A0-EAAYASAAEgKQU_D_BwE" target="_blank"> <button type="button" class="btn btn-primary">Reservar </button></a> 
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card toys">
                    <img src="/assets/img/morada-do-mar.jpg" class="img-thumbnail" alt="...">
                    <div class="card-body text-center">
                      <h4> Morada do Mar Hotel</h4>
                      <h6> <i class="fas fa-map-marker-alt"></i> Bombinhas-SC</h6>
                      <h6> <i class="fas fa-phone-alt"></i>  (47) 3393-6090 </h6>
                      <p>É dentro de um espírito de preservação, cuidado e hospitalidade que o Hotel Morada do Mar abre suas portas na beira mar de uma das praias mais paradisíacas do litoral brasileiro.</p>
                        <a href="https://moradadomar.com.br/" target="_blank"> <button type="button" class="btn btn-primary">Reservar </button></a> 
                    </div>
                  </div>
                </div>


              </div>
            </section>


    <?php  include('footer.php'); ?>

<?php  include('whatsicon.html'); ?>